Proper yaml for images
```
tileImage: /images/
imageUrl: craft/
imageTitle: 
transparent: false
```
---

Add images 
```
![alt text](/images/folder/filename.jpg)
```

Add images as a Link
[![alt text](/images/folder/filename.png)](https://link/)

---

add zoom
```
<figure><img src="/images/folder/filename.jpg" data-action="zoom"/></figure>
```
---
**add video:**

```
<video id=""
    src="/images/"
    width="" height=""
    preload="auto" 
    autoplay loop controls 
    style="max-width: 100%; width: auto !important;height: auto !important;"
    poster="/images/" title="Tap to play/pause">
</video>
```

**Add things to a series**

for eg: add 
```
featuredTag:
  - platyfight
```

to the yaml, including the same name in the tag list you want as the series name.

---


Link to items online but aren't visible on the site
```
unlisted: true
```
---

for custom menu

https://gohugo.io/content-management/menus/


centre text 

<p style="text-align: center;">Text</p>



<u>TO DO</u>

increase main column width by default, put commentary in line

---
Sell something! (the product name is the page title)
```
purchaseNow: true
productDescription: A description
purchaseAmount: 250
```

TINA CMS update

`nix-shell -p nodejs_20` for using tina locally


`npx tinacms dev -c "hugo server"` for dev/test

(still need to sort tags)

carry on updating config.ts with content schema for collections. 


---

2024 UPDATE

Change yaml in archetype for images to point to a holding image so it doesn't break when it serves.

note - when making a new post, it will throw an error, typora denied. It still creates the post, open in something else.

--

Cloudflare page deployment can be checked here: https://dash.cloudflare.com/ea9b9229062e95187906baab69ee9915/pages/view/portfolio 

--

Analytics are available here: https://us.posthog.com/project/70763
and here: https://doin-numbers.qwrk.dev/platymoose.ninja
(anon view at https://doin-numbers.qwrk.dev/share/platymoose.ninja?auth=LqR9UQhh2R4gxx1thK_NN )

#TODO:
- ~~correct PUG templates to match HTML~~ 
- ~~(ADD POSTHOG)~~
- ~~CORRECT CSP~~
- Add custom domain to analytics ala https://github.com/PostHog/posthog.com/blob/master/contents/docs/advanced/proxy/cloudflare.mdx
