---
date: 2017-02-28T18:41:47.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/bag-wolf.jpg
share: true
slug: purse-wolf
tags:
- felt
- hand-made
- bag
- animals
- wolf
- available
- forest-thingamajigs
- strawberry-butts
tileImage: /tiles/bag-wolf_tile.jpg
title: Wolf Purse
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


One of my felt creations, Canis Lupus Feltus! 

Hand stitched, soft plushy fabric, button eyes and furry cheeks, with a convenient magnet fastening. Large enough to hold earphones or change, small enough to hide in your bag (about 11cm x 11cm.)

Available to buy