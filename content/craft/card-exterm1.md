---
date: 2017-02-25T21:04:09.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/card-exterm1.jpg
share: true
slug: card-exterm
tags:
- watercolor
- pen-drawing
- card
- hand-painted
- hand-made
- gift
- dr-who
tileImage: /tiles/card-exterm1_tile.jpg
title: EXTERMINATE
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


Inside the card
<figure><img src="/images/craft/card-exterm2.jpg" data-action="zoom"/></figure>

**Medium**: Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces. Including fanart stuffs. 
This was one of my favooorites