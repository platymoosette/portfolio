---
date: 2017-03-03T22:26:08.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/bowl-koi.jpg
share: true
slug: bowl-koi
tags:
- acrylic
- glass
- hand-painted
- animals
- koi-fish
tileImage: /tiles/bowl-koi_tile.jpg
title: Koi Bowl
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


Medium: A glass bowl, painted with acrylic paints and varnished with resin
