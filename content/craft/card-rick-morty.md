---
date: 2017-02-25T21:05:29.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/card-rick-morty.jpg
share: true
slug: rick-morty
tags:
- watercolor
- card
- hand-painted
- hand-made
- gift
- pen-drawing
- rick-and-morty
- rick-sanchez
tileImage: /tiles/card-rick-morty_tile.jpg
title: Rick and Morty
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Watercolor, pen and ink on card.

**Where it went:** My S.O's desk, reminding him to stay schwifty.