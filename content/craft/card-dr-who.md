---
date: 2017-02-25T21:04:08.0000000+02:00
description: A wild Tardis appears!
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/card-dr-who.jpg
share: true
slug: dr-who
tags:
- watercolor
- pen-drawing
- card
- gift
- hand-painted
- hand-made
- dr-who
tileImage: /tiles/card-dr-who_tile.jpg
title: Tardalek?
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces, which this is one of. I had great fun designing whimsical cards that could surprise-cheer-up someones day.

