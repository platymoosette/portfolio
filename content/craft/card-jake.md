---
date: 2017-02-25T21:05:20.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/card-jake.jpg
share: true
slug: jake
tags:
- watercolor
- pen-drawing
- card
- hand-painted
- hand-made
- gift
- adventure-time
tileImage: /tiles/card-jake_tile.jpg
title: a Jake Hug
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Watercolor and pen on card.

**Where it went:** Hanging on my wall, sending me encouragement through adventure time awesomeness.