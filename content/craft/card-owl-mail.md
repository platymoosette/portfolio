---
date: 2017-02-25T21:04:48.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/card-owl-mail.jpg
share: true
slug: owl-mail
tags:
- pen-drawing
- card
- hand-painted
- hand-made
- gift
- harry-potter
- owl
tileImage: /tiles/card-owl-mail_tile.jpg
title: Owl Mail
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces. This is one of my favorites! Who doesn't want to get owl mail? 