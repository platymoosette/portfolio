---
date: 2017-02-26T20:48:38.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/card-fist-bump.jpg
share: true
slug: fist-bump
tags:
- watercolor
- pen-drawing
- card
- hand-painted
- hand-made
- gift
- adventure-time
tileImage: /tiles/card-fist-bump_tile.jpg
title: Fist Bump!
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces, which this is one of. I had great fun designing whimsical cards that could surprise-cheer-up someones day.
