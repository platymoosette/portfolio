---
date: 2017-02-25T21:04:40.0000000+02:00
description: yer a wizard
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/card-hagrid.jpg
share: true
slug: hagrid
tags:
- pen-drawing
- card
- hand-painted
- hand-made
- gift
- harry-potter
tileImage: /tiles/card-hagrid_tile.jpg
title: Hagrid
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces, which this is one of. I particularly liked the idea of sending a big, grizzly, safe, hagrid hug to someone. Totally consensual, of course.