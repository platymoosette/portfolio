---
date: 2017-02-26T21:13:09.0000000+02:00
description: Day of the dead inspired box of holding! Sugar skulls and spanish language mashup
draft: false
featured: false
imageTitle: Cosas Muertas - Dead Things
imageUrl: /images/craft/box-skull.jpg
share: true
slug: box-skull
tags:
- acrylic
- box
- hand-painted
- sugar-skull
- gift
tileImage: /tiles/box-skull_tile.jpg
title: Sugar Skull Box
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


## *Más dulce que la muerte* - Sweeter than death

<figure><img src="/images/craft/box-skull-dulce.jpg" data-action="zoom"/></figure>

## *Cráneos y dulces* - skulls and candy

<figure><img src="/images/craft/box-skull-craneos.jpg" data-action="zoom"/></figure>

**Medium**: Hand painted in acrylic.

**Where it went**: A gift that is now holding macabre trinkets on a shelf somewhere, for someone wonderfully odd.

Another piece that I got to inject some other language in to. Yay