---
date: 2020-12-27T21:10:43+02:00
description: ""
draft: false
featured: false
tileImage: "/tiles/piano-prac-ff-noctis-tile.jpg"
share: false
slug: piano-ff-noctis
tags:
- piano
- final-fantasy
- video
title: Final Fantasy - Noctis
---

I've started learning piano over the last few months, and this was one of my favourite pieces to learn. It really challenged my finger coordination and stretch... 
I haven't yet nailed the ending, but other than that, this was the best take I managed to record. 

Nothing better than playing music from your favourite games !

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/5021450be15e4620b0a40e973974176c?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>	
