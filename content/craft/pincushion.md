---
date: 2017-06-03T19:57:11.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: 
share: false
slug: owl-pincushion
tags:
- hand-made
- craft
- sewing
- gift
- felt
- owl
- animals
tileImage: /tiles/pin-owl-front_tile.jpg
title: Owl Pincushion
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


Another gift! This time a rather clever pincushion idea as an adorable owl.   

<figure><img src="/images/craft/pin-owl-front.jpg" data-action="zoom"/> </figure>

The pincushion was hand stitched using felt and material.
<figure><img src="/images/craft/pin-owl-side.jpg" data-action="zoom"/> </figure>

The band was made from altering a shirt cuff and sewing a strip of magnets into the side to grab any stray needles! That was the clever part. 


Finally fastened with velcro and decorated with bebeh buttons. 


<figure><img src="/images/craft/pin-owl-inbox.jpg" data-action="zoom"/></figure>

Definitely a cutie patootie design I enjoyed making