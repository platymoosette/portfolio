---
date: 2017-02-28T18:41:50.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/purse-fox.jpg
share: true
slug: purse-fox
tags:
- hand-made
- craft
- bags
- animals
- fox
- forest-thingamajigs
- strawberry-butts
- available
tileImage: /tiles/purse-fox_tile.jpg
title: Fox Purse
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

Another lil forest friend thingamajig! 

My hands like to remember art through texture and craft sometimes, and letting them figure out what they want to do is kinda therepeutic to the rest of me. I forget how digital art isn't as tactile as my traditionally trained brain is used to...

So I dug out some left over fabrics from previous projects and threw them in the brain couldron to see what would come out. Apparently little foxes with strawberry butts ? Made into bags! 

This little potato still holds my earphones and spare doodads