---
date: 2017-02-28T19:09:31.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/box-black-bird.jpg
share: true
slug: box-black-bird
tags:
- acrylic
- box
- hand-painted
- cherry-blossom
- animals
- birds
- gift
tileImage: /tiles/box-black-bird_tile.jpg
title: Cherry Blossom Bird
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

**Medium**: A box of chocolates repainted in Acrylic.

The back
<figure><img src="/images/craft/box-black-2.jpg" data-action="zoom"/></figure>

The top
<figure><img src="/images/craft/box-black-top.jpg" data-action="zoom"/></figure>


An enjoyable and tasty project, all in all. Om nom nom 
