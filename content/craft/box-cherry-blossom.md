---
date: 2017-02-28T22:34:32.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/box-cherry-blossom.jpg
share: true
slug: box-cherry-blossom
tags:
- acrylic
- box
- hand-painted
- animals
- cherry-blossom
- japanese
- gift
tileImage: /tiles/box-cherry-blossom_tile.jpg
title: Cherry Blossom
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

<figure><img src="/images/craft/box-cherry-birthday.jpg" data-action="zoom"/><figcaption>tanjoubi omedetou - happy birthday</figcaption></figure>

<figure><img src="/images/craft/box-cherry-love.jpg" data-action="zoom"/><figcaption>takusan no ai - lots of love</figure>

Another in the series of repurposing boxes by painting on them.