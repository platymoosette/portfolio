---
date: 2017-02-25T21:04:35.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/card-hogwarts.jpg
share: true
slug: hogwarts
tags:
- watercolor
- card
- pen-drawing
- hand-painted
- hand-made
- gift
- harry-potter
tileImage: /tiles/card-hogwarts_tile.jpg
title: Hogwarts
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces, which this is one of. 
I had great fun designing whimsical cards that could surprise-cheer-up someones day. 