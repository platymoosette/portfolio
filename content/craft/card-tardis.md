---
date: 2017-02-26T20:46:01.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/card-tardis.jpg
share: true
slug: tardis
tags:
- watercolor
- pen-drawing
- card
- hand-painted
- hand-made
- gift
- pen-drawing
- dr-who
tileImage: /tiles/card-tardis_tile.jpg
title: Timey Wimey
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

**Medium:** Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces. This is the one that got away, and is still lying in my drawer waiting for an owner!
Who wants a tardis in the mail?

