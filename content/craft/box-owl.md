---
date: 2017-02-28T22:34:18.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: Quelques chose - Some things
imageUrl: /images/craft/box-owl.jpg
share: true
slug: box-owl
tags:
- acrylic
- box
- hand-painted
- animals
- owl
- french
- gift
tileImage: /tiles/box-owl_tile.jpg
title: La Chouette
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

## *La chouette et la lune* - the owl and the moon
<figure><img src="/images/craft/box-owl-lune.jpg" data-action="zoom"/></figure>

## *Sous les etoiles* - under the stars
<figure><img src="/images/craft/box-owl-etoiles.jpg" data-action="zoom"/></figure>


This was the piece that got me started on the idea of repurposing old boxes and decorating them. 