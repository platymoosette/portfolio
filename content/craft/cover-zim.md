---
date: 2017-02-26T21:13:19.0000000+02:00
description: My tallest!
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/cover-zim.jpg
share: true
slug: cover-zim
tags:
- acrylic
- hand-painted
- phone-cover
- invader-zim
tileImage: /tiles/cover-zim_tile.jpg
title: Zim Phone Cover
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Acrylic paint on a Galaxy phone cover. Sprayed with matt varnish.
