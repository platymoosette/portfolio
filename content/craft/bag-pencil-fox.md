---
date: 2017-02-28T18:41:33.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/bag-pencil-fox.jpg
share: true
slug: bag-pencil-fox
tags:
- felt
- hand-made
- bag
- animals
- fox
- traditional
- craft
- available
- forest-thingamajigs
- strawberry-butts
tileImage: /tiles/bag-pencil-fox_tile.jpg
title: Fox Pencil Bag
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

**Medium**: Hand stitched felt bag, with sewn inner lining. Magnets sewn into the nose flap to close it.

**Size**: 20cm x 15cm - large enough to hold a bunch of pencils.

**Where it went**: The original follows me around with my art supplies, like a cute bag of holding.

