---
date: 2017-03-03T22:26:01.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/craft/grass-dragon.jpg
share: true
slug: grass-dragon
tags:
- acrylic
- sculpture
- hand-painted
- fantasy
- dragon
tileImage: /tiles/grass-dragon_tile.jpg
title: Grass Dragon
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

<figure><img src="/images/craft/grass-dragon-2.jpg" data-action="zoom"/></figure>


**Medium:** Fired pottery painted in acrylic. 

**Where it went:** It now lives in the garden, blending in with the thyme like a true mythical beast.