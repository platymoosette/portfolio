---
date: 2017-07-26T17:34:53+02:00
description: ""
draft: false
featured: false
imageTitle: null
imageUrl: 
share: false
slug: princess-maya
tags:
- animal-portrait
- digital-animal-portrait
- digital-painting
- animals
- dogs
- fantasy
- commission
- star-wars
- leia
- cv
tileImage: /images/painting/digi-maya-tile.jpg
title: Princess Maya
transparent: false
---
I had the joy of working with a wonderful client on a special commission this month. It was going to be a normal portrait series of their dogs as a birthday gift, and turned into finding inspiration from 80's sci fi and fantasy movies to create portraits with a twist

Meet Maya, a cheeky, fun loving, no-nonsense fluff ball nicknamed "little princess" by her humans. 

<figure><img src="/images/painting/digi-maya.jpg" data-action="zoom"/></figure>

Nothing fitted that description better than the Rebel Princess herself, Leia Organa from Star Wars!

Here are some closeups:

<figure><img src="/images/painting/digi-maya-face.jpg" data-action="zoom"/></figure>

I really enjoyed painting her curly fur and adding in stray hairs and whiskers.

<figure><img src="/images/painting/digi-maya-chest.jpg" data-action="zoom"/></figure>

This was one of the first paintings I've done in black and white and added colour in afterwards using gradient maps and overlay layers. I decided to try it because both the dogs were mainly black and I wanted to play with reflected colour to add interest and mood. 

Here's what it looked like before I added colour: 

<figure><img src="/images/painting/digi-maya-bw.jpg" data-action="zoom"/></figure>

All done digitally using Clip Studio Paint, some custom brushes and my Wacom Mobilestudio Pro.

Printed A3 on 350gsm paper, framed and hand delivered!

<figure><img src="/images/painting/comissionphoto.jpg" data-action="zoom"/></figure>

