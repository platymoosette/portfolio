---
date: 2017-02-25T20:38:09+02:00
description: "Capturing the lighter side to my cat"
draft: false
featured: false
tileImage: /images/painting/acrylic-kits1.jpg
imageUrl: /images/painting/acrylic-kits2.jpg
imageTitle: 
transparent: false
share: true
slug: acrylic-kits1
tags:
- acrylic
- traditional
- painting
- animal-portrait
- cat
- animals
- kitsune
title: Angelic Cat
---
**Medium:** Acrylic paint on board

**Size:** 21cm x 29cm - A4