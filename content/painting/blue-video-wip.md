---
date: 2021-01-08T12:10:43+02:00
description: "A short timelapse video to show how I paint"
draft: false
featured: false
tileImage: "/tiles/digi-blue-tile2.jpg"
imageUrl: 
imageTitle:
transparent: false
share: false
slug: blue-wip
tags:
- wip
- dogs
- fantasy
- video
- commission
title: Blue Process Video
---

<video 
    src="/images/painting/dogvids/blue-process-vid.mp4"
    autoplay loop controls 
    width="863" height="1264"
    poster="/tiles/digi-blue-tile2.jpg"
    style="max-width: 100%;"
    title="Tap to play/pause">
</video>

You can find the finished painting [here](https://platymoose.ninja/painting/little-prince/)

[![blue](/images/painting/digi-blue.jpg)](https://platymoose.ninja/painting/little-prince/)

