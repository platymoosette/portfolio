---
date: 2017-07-26T17:34:43+02:00
description: ""
draft: false
featured: false
imageTitle: null
imageUrl: /images/painting/digi-blue.jpg
share: false
slug: little-prince
tags:
- animal-portrait
- digital-animal-portrait
- digital-painting
- animals
- dogs
- fantasy
- commission
- aragorn
- lord-of-the-rings
- cv
tileImage: /images/painting/digi-blue-tile.jpg
title: Little Prince
transparent: false
---

I had the joy of working with a wonderful client on a special commission this month. It was going to be a normal portrait series of their dogs as a birthday gift, and turned into finding inspiration from 80's sci fi and fantasy movies to create portraits with a twist! 

This is Blue, a regal yet rugged, intelligent hound nick named "little prince" by his human.

I was inspired by Aragorn from Lord of the Rings, a kind, rough, muddy secret king! Light, bright forest colours seemed apt for the palette, and I added in the cloak, leaf pin and crown for some character reference.

Here are some close ups:
<figure><img src="/images/painting/digi-blue-muzz.jpg" data-action="zoom"/></figure>

I'm a fan of intricate work, so I really enjoyed painting in texture and little details like whiskers and hairs <3 

<figure><img src="/images/painting/digi-blue-chest.jpg" data-action="zoom"/></figure>

This was one of the first paintings I've done in black and white and added colour in afterwards using gradient maps and overlay layers. I decided to try it because both the dogs were mainly black and I wanted to play with reflected colour to add interest and mood. It was a huge learning curve both for my painting techniques and my portrait skills but totally worth it and a technique I want to keep using. 

This is how it was looking before I added colour: 

<figure><img src="/images/painting/digi-blue-bw.jpg" data-action="zoom"/></figure>

All done digitally using Clip Studio Paint, some custom brushes and my Wacom Mobilestudio Pro.

Printed A3 on 350gsm paper, framed and hand delivered! :)

<figure><img src="/images/painting/comissionphoto.jpg" data-action="zoom"/></figure>
