---
date: 2017-02-27T17:26:14.0000000+02:00
description: Matric Final part 2
draft: false
featured: false
imageTitle: 
imageUrl: /images/painting/chalk-skull.jpg
share: true
slug: chalk-skull
tags:
- traditional
- chalk-pastel
- still-life
- skull
tileImage: /tiles/chalk-skull_tile.jpg
title: Skull Life
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

My second piece in chalk pastel, a more sombre still life. That wine glass was my nemesis...

**Medium:** Chalk pastel on paper

**Size:**  42 x 59 cm - A2