---
date: 2020-12-26T17:09:41+02:00
description: ""
draft: false
featured: false
imageTitle: null
imageUrl: /painting/dogvids/bey-wip.jpg
share: false
slug: beyla-wip
tags:
- animal-portrait
- digital-animal-portrait
- digital-painting
- animals
- dogs
- video
- wip
- rottweiler
tileImage: /tiles/bey-wip-tile3.jpg
title: Beyla WIP
transparent: false
---

<video id="beyla"
    src="/images/painting/dogvids/rottiewip.mp4"
    width="719" height="900"
    preload="auto" 
    autoplay loop controls 
    style="max-width: 100%; width: auto !important;height: auto !important;"
    poster="/images/painting/dogvids/bey-wip.jpg" title="Tap to play/pause">
</video>