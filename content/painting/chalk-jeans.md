---
date: 2017-02-27T17:26:00.0000000+02:00
description: Matric Final part 1
draft: false
featured: false
imageTitle: 
imageUrl: /images/painting/chalk-jeans.jpg
share: true
slug: chalk-jeans
tags:
- traditional
- chalk-pastel
- still-life
tileImage: /tiles/chalk-jeans_tile.jpg
title: Art Still Life
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

My first piece in chalk pastel, a still life representing me. Jeans, art stuff and my coffee cup … What else do I need?

**Medium:** Chalk pastel on paper

**Size:**  42 x 59 cm - A2
