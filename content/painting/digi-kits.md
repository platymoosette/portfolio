---
date: 2017-02-25T19:02:22.0000000+02:00
description: My cat, kitsune, sitting in the sun being very photogenic.
draft: false
featured: false
imageTitle: why is he so beautiful
imageUrl: /images/painting/digi-kits.jpg
share: true
slug: digi-kits
tags:
- digital-animal-portrait
- animal-portrait
- digital-painting
- animals
- cat
- kitsune
- cv
tileImage: /tiles/digi-kits_tile.jpg
title: Sun Fox
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

<figure><img src="/images/painting/digi-kits-crop.jpg" data-action="zoom"/></figure>

**Medium:** Digital, Adobe Photoshop, Wacom Intuos3