---
date: 2018-08-02T12:21:05+02:00
description: ""
draft: false
featured: true
imageTitle: null
imageUrl: images/painting/vampcat/vampirecat.jpg
share: false
slug: vampire-cat
tags:
- animal-portrait
- digital-animal-portrait
- digital-painting
- animals
- cats
- fantasy
- commission
- vampire
- ghost
- cv
tileImage: /images/painting/vampcat/vampirecat-small.jpg
title: Vampire Cat
transparent: false
---

Meet Catface (yes, his real name), a majestic oriental with amazing fangs and such a personality. I was lucky enough to be told to follow my inspiration and paint Catface as I saw him. Oh such dangerous words to an odd soul like me !

His fangs were definitely a feature I couldn't ignore, and reviewing the reference photos revealed a duality to him that was hilarious and spooky. I just had to portray the two sides to his personality and show off his toofs.

A great challenge that pushed me to play with light, texture and dimension.

<figure><img src="/images/painting/vampcat/vampirecat-crop.jpg" data-action="zoom"/></figure>

Here are some process shots of the idea developing.

First some sketches before I had a solid idea. They had mentioned him being a debonnaire romantic type and expressed interest in a steampunk theme. 

<figure><img src="/images/painting/vampcat/vampcat-compalt1.jpg" data-action="zoom"/></figure>

<figure><img src="/images/painting/vampcat/vampcat-compalt2.jpg" data-action="zoom"/></figure>

But seeing as they were more open to my interpretation of his character, I decided to develope this ghosty idea

<figure><img src="/images/painting/vampcat/vampcat-comp-1.jpg" data-action="zoom"/></figure>


<figure><img src="/images/painting/vampcat/vampcat-comp-2.jpg" data-action="zoom"/></figure>

<figure><img src="/images/painting/vampcat/vampcat-comp-3.jpg" data-action="zoom"/></figure>

Some photos of the final print on high quality paper

<figure><img src="/images/painting/vampcat/vampcat-print-crop.jpg" data-action="zoom"/></figure>

<figure><img src="/images/painting/vampcat/vampcat-print-paper.jpg" data-action="zoom"/></figure>

I also tried a new form of printing, using transparent sheets of acrylic with ink printed inbetween, which gave a really cool and impressive translucent effect to the ghostly cat. Better seen in person though.

<figure><img src="/images/painting/vampcat/vampcat-acrylicprint.jpg" data-action="zoom"/></figure>
