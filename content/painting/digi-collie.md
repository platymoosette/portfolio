---
date: 2017-07-22T18:09:25.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: Obligitary comment about someone down a well.
imageUrl: /images/painting/digi-lassie.jpg
share: false
slug: collie
tags:
- animal-portrait
- digital-animal-portrait
- digital-painting
- animals
- dogs
- video
tileImage: /tiles/digi-lassie_tile2.jpg
title: Collie Portrait
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

Some practice before a commission was needed, it's been a while since I've painted fur (first time in Clip Studio) and I wanted to find a roughish style that could work. So I tried out some new brushes and played around. 

Have some WIP footage of the pupperfication

<video id="pupperfication"
    src="/images/painting/digi-lassie-wip.mp4"
    width="719" height="900"
    preload="auto" 
    autoplay loop controls 
    style="max-width: 100%; width: auto !important;height: auto !important;"
    poster="/images/painting/digi-lassie.jpg" title="Tap to play/pause">
</video>