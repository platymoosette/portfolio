---
date: 2017-02-25T19:02:26.0000000+02:00
description: ''
draft: false
featured: true
imageTitle: Et ses cheveux étaient feu
imageUrl: /images/painting/digi-roux.jpg
share: true
slug: digi-roux
tags:
- portrait
- painted-portrait
- digital-painting
- wip
- cv
tileImage: /tiles/digi-roux_tile.jpg
title: Cheveux de feu
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

**Medium:** Digital, Adobe Photoshop, Wacom Intuos3

Exploring faces and textures.

## The Work in progress

<figure><img src="/images/painting/digi-roux-wip2.jpg" data-action="zoom"/></figure>

