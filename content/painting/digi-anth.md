---
date: 2017-02-25T19:01:58.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: Played by dear sir Anthony Hopkins
imageUrl: /images/painting/digi-anth.jpg
share: true
slug: digi-anth
tags:
- digital-painting
- painted-portrait
- portrait
- tribute
- anthony-hopkins
- the-elephant-man
tileImage: /tiles/digi-anth_tile.jpg
title: Frederick Treves
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

**Medium:** Digital, Adobe Photoshop, Wacom Intuos3

Inspired by watching the Elephant Man (one of my favorite movies!) I couldn't resist capturing Anthony's face in this clip

<figure><img src="/images/painting/digi-anth-wip.jpg" data-action="zoom"/></figure>