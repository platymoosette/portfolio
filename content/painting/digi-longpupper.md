---
date: 2019-01-28T18:03:02+02:00
description: ""
draft: false
featured: true
imageTitle: null
imageUrl: /images/painting/dogvids/longface-wip.jpg
share: false
slug: long-pupper
tags:
- animal-portrait
- digital-animal-portrait
- digital-painting
- animals
- dogs
- afghan-hound
- video
- cv
tileImage: /tiles/longface-tile2.jpg
title: Long pupper
transparent: false
---

<video id="long pupper"
    src="/images/painting/dogvids/longfaceprocess.mp4"
    width="600" height="848"
    preload="auto" 
    autoplay loop controls 
    style="max-width: 100%; width: auto !important;height: auto !important;"
    poster="/images/painting/dogvids/longfaceFINIT.jpg" title="Tap to play/pause">
</video>

<figure><img src="/images//painting/dogvids/longfaceFINIT.jpg" data-action="zoom"/></figure>


