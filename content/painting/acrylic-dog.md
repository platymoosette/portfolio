---
date: 2017-02-25T20:38:00.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/painting/acrylic-dog.jpg
share: true
slug: acrylic-dog
tags:
- acrylic
- dogs
- traditional
- painting
- animal-portrait
- animals
tileImage: /tiles/acrylic-dog_tile.jpg
title: Basset Hound
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

**Medium:** Acrylic paint on board

**Size:** 21cm x 29cm - A4
