---
date: 2017-02-25T20:38:07.0000000+02:00
description: Capturing the darker side of my cat (far easier than the other…)
draft: false
featured: false
imageTitle: 
imageUrl: /images/painting/acrylic-kits2.jpg
share: true
slug: acrylic-kits2
tags:
- acrylic
- traditional
- painting
- animal-portrait
- cat
- animals
- kitsune
tileImage: /tiles/acrylic-kits2_tile.jpg
title: Shadow cat
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

**Medium:** Acrylic paint on board

**Size:** 21cm x 29cm - A4