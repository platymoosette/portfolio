---
date: 2017-02-25T19:02:13.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: My cousins
imageUrl: /images/painting/digi-sara.jpg
share: true
slug: digi-sara
tags:
- portrait
- painted-portrait
- digital-painting
- commission
- cv
tileImage: /tiles/digi-sara_tile.jpg
title: Boucle d'or
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

**Medium:** Digital, Adobe Photoshop, Wacom Intuos3

A baby brother was born and welcomed