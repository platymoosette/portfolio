---
date: 2017-02-25T19:01:51.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: I can't remember if she liked flowers or not
imageUrl: /images/painting/digi-shay.jpg
share: true
slug: digi-shay
tags:
- portrait
- painted-portrait
- digital-painting
- gift
- cv
tileImage: /tiles/digi-shay_tile.jpg
title: Shayaan
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

**Medium:** Digital, Adobe Photoshop, Wacom Intuos3

A gift for a special friend