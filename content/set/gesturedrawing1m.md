---
date: 2018-04-09T14:10:04+02:00
description: This time with feeling!
draft: false
featured: false
featuredTray: true
share: true
slug: gesture-drawing-1minute
tags:
- sketch
- figure-study
- practice
- series
- wip
title: 1 minute Gesture Drawing
unlisted: false
showGalleryTitles: false
tileImage: /images/set/gesturedrawing/gesture-3a.jpg
gallery:
  - title:
    src: /images/set/gesturedrawing/gesture-2a.jpg
  - title:
    src: /images/set/gesturedrawing/gesture-2b.jpg
  - title:
    src: /images/set/gesturedrawing/gesture-2c.jpg
  - title:
    src: /images/set/gesturedrawing/gesture-3a.jpg
  - title: 
    src: /images/set/gesturedrawing/gesture-3b.jpg
  - title: 
    src: /images/set/gesturedrawing/gesture-3c.jpg
  - title: 
    src: /images/set/gesturedrawing/gesture-3d.jpg
---
Another challenge, this time I wanted to try capture more motion from action poses, 1 minute each
