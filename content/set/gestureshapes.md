---
date: 2018-04-08T18:30:17+02:00
title: Gesture Shapes
description: 5 minute observations
draft: false
featured: false
featuredTray: true
share: true
slug: gesture-shapes
tags:
- sketch
- figure-study
- practice
- series
unlisted: false
showGalleryTitles: false
tileImage: /images/set/gesturedrawing/lifedrawing-4a.jpg
gallery:
  - title:
    src: /images/set/gesturedrawing/fold1.jpg
  - title:
    src: /images/set/gesturedrawing/fold2.jpg
  - title:
    src: /images/set/gesturedrawing/fold3.jpg
  - title:
    src: /images/set/gesturedrawing/dance1.jpg
  - title: 
    src: /images/set/gesturedrawing/dance2.jpg
---

Here I was trying to visually identify the shapes going into poses and motion, and reiterating them.