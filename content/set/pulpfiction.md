---
date: 2018-04-09T14:09:46+02:00
description: 'Now I wanna dance, I wanna win. I want that trophy, so dance good!'
draft: false
featured: false
featuredTray: true
share: true
slug: pulp-fiction-shapes
tags:
- sketch
- figure-study
- practice
- series
- wip
- pulp-fiction
title: Pulp Fiction drawings
unlisted: false
showGalleryTitles: false
tileImage: /images/set/gesturedrawing/pulp-tile.jpg
gallery:
  - title:
    src: /images/set/gesturedrawing/jon1.jpg
  - title:
    src: /images/set/gesturedrawing/jon2.jpg
  - title:
    src: /images/set/gesturedrawing/jon3.jpg
  - title:
    src: /images/set/gesturedrawing/jon4.jpg
  - title:
    src: /images/set/gesturedrawing/mia1.jpg
  - title:
    src: /images/set/gesturedrawing/mia2.jpg
  - title:
    src: /images/set/gesturedrawing/mia3.jpg
  - title:
    src: /images/set/gesturedrawing/mia4.jpg
  - title:
    src: /images/set/gesturedrawing/mia5.jpg
---

Gesture drawing practice! Breaking apart shapes and lines and reiterating. This time with a throwback reference to one of my favorite movies