---
date: 2018-04-08T17:55:55+02:00
description: draw me like one of your french girls
draft: false
featured: false
featuredTray: true
share: true
slug: gesture-drawing-30seconds
tags:
- sketch
- figure-study
- practice
- series
- wip
title: Gesture Drawing
unlisted: false
showGalleryTitles: false
tileImage: /images/set/gesturedrawing/gesture-1a.jpg
gallery:
  - title:
    src: /images/set/gesturedrawing/gesture-1a.jpg
  - title:
    src: /images/set/gesturedrawing/gesture-1b.jpg
  - title:
    src: /images/set/gesturedrawing/gesture-1c.jpg
  - title:
    src: /images/set/gesturedrawing/gesture-1d.jpg
  - title: 
    src: /images/set/gesturedrawing/gesture-1e.jpg
  - title: 
    src: /images/set/gesturedrawing/gesture-1f.jpg
  
---

I've been doing an online gesture drawing class that challenges you to capture certain elements of a live model within different time frames. These were 30seconds to a minute.