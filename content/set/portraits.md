---
comments: true
date: 2018-04-25T23:24:39+02:00
description: 15 minute drawings of different facial expressions
draft: false
featured: false
featuredTray: true
share: true
slug: portrait-exercise
tags:
- sketch
- figure-study
- practice
- series
- wip
- portrait
title: Portrait Exercise
unlisted: false
showGalleryTitles: true
imageUrl: /images/set/portpractice1.jpg
tileImage: /images/set/portpractice1-tile.jpg
gallery:
  - title: Resting face
    src: /images/set/portpractice1.jpg
  - title: Smiling
    src: /images/set/portpractice2.jpg
  - title: Smiling wip
    src: /images/set/portpractice2lines.jpg
  - title: Wuh
    src: /images/set/portpractice3.jpg
  - title: Nuh uh
    src: /images/set/portpractice4.jpg
  - title: Nuh uh wip
    src: /images/set/portpractice4lines.jpg
  - title: Rawr
    src: /images/set/portpractice5.jpg
  - title: Rawr wip
    src: /images/set/portpractice5lines.jpg
  - title: Derp
    src: /images/set/portpractice6.jpg
  - title: Derp wip
    src: /images/set/portpractice6lines.jpg
  - title: Increduderp
    src: /images/set/portpractice7.jpg
  - title: Increduderp wip
    src: /images/set/portpractice7line.jpg
  - title: Squee
    src: /images/set/portpractice8.jpg
  - title: Squee wip
    src: /images/set/portpractice8line.jpg

---
I've been trying to challenge myself with different drawing practices, and have found time limits strangely useful. This time I was capturing facial expressions and working on loosening up my line work without losing accuracy and character. I had a lot of fun with it though, and enjoyed seeing how much expression changes the shape of someones face.

I tried to keep these to 15 - 20 minutes long. 



