---
featuredTag:
  - memories
title: Memories - Music
description: ''
tags:
  - illustration
  - video
  - memories
  - in-memory-of
  - ipad-pro
  - procreate
  - symbology
date: 2020-12-26T15:10:43.000Z
draft: false
featured: false
tileImage: /tiles/memories-music-tile2.jpg
imageUrl: /images/illustration/memories/memories-music.jpg
imageTitle: null
transparent: false
share: false
slug: memories-music
---

It's been so long since I could ask you anything.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/4c07c3466c9a420b9829ceded513571c?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>
