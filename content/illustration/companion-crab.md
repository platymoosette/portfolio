---
date: 2018-04-05T14:05:43.0000000+02:00
description: Don't worry, shes been de-beaked!
draft: false
featured: true
imageTitle: 
imageUrl: /images/illustration/companioncrab-trans-small.png
share: false
slug: companion-crab
tags:
- illustration
- sticker
- fancon
- half-life
- portal
- valve
- wacom-mobile-studio-pro
- clip-studio-paint
- cv
tileImage: /tiles/companioncrab-trans-small_tile.png
title: Companion Crab
transparent: true
comments: false
featuredTag: 
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


Just a lil sneak peak of one of my brain farts for Fancon 2018, where Lamarr and her companion cube will be available in sticker form.

Lamarr is one of my favorite characters from half life :3 well, from anything ever I guess.

some progress shots of the idea

<figure><img src="/images/illustration/companioncrab1.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/companioncrab2.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/companioncrab3.jpg" data-action="zoom"/></figure>
