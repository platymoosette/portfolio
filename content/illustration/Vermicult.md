---
title: Vermicult
description: We all belong to the worms
date: 2024-05-14T19:51:49.059Z
draft: false
featured: true
tileImage: /tiles/Vermicult_tile.jpg
imageUrl: /images/illustration/Vermicult.jpg
imageTitle: ''
transparent: false
share: false
slug: vermicult
tags:
- cv
---

Poster design to raise awareness about the magic of wormi-composting
