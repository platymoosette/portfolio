---
date: 2018-04-05T13:08:43.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/addy-fin.jpg
share: false
slug: addy-carver
tags:
- digital
- portraiture
- z-nation
- zombies
- illustration
- portrait
- z-nation
- zombies
- fancon
- wacom-mobile-studio pro
- clip-studio-paint
tileImage: /tiles/addy-fin_tile.jpg
title: Addison Carver
transparent: false
comments: false
featuredTag:
  - z-nation
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


One of my favorite characters from Z nation. More importantly, she has one of my favorite weapons, the Z-whacker! 

Created on my Wacom Mobile Studio Pro and with a zombie addiction running rampant 

<video id="addy"
    src="/images/illustration/angryaddyprocess.mp4"
    width="500" height="607"
    preload="auto" 
    autoplay loop controls 
    style="max-width: 100%; width: auto !important;height: auto !important;"
    poster="/images/illustration/addycarver-small.jpg" title="Tap to play/pause">

What she looked like before texture was added

<figure><img src="/images/illustration/addycarver.jpg" data-action="zoom"/></figure>


