---
title: Wolf Forest
description: Giving princess mononoke vibes
date: 2024-05-14T20:19:00.070Z
draft: false
featured: false
tileImage: /tiles/wolf-forest_tile.jpg
imageUrl: /images/illustration/wolf-forest.jpg
transparent: false
share: false
slug: wolf-forest
tags:
- cv
---

I've been playing around with more kid friendly, whimsical illustration styles so I can focus on illustrating for kids books and comics more. Which is something I've always wanted to try but never got around to (except for those educational kids books I did when I was 19...weird) 
