---
date: 2017-06-03T17:37:06.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: Harry Romano
imageUrl: /images/illustration/mugshots/butcher.jpg
share: false
slug: butcher
tags:
- illustration
- portrait
- mugshot
- fancon
- wacom-mobile-studio-pro
- clip-studio-paint
- cv
tileImage: /tiles/butcher_tile.jpg
title: The Butcher
transparent: true
comments: false
featuredTag:
  - mugshot
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Digital, Clip Studio Paint, Wacom Mobile Studio Pro

