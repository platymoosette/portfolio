---
date: 2019-01-09T13:20:09.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/digi-corgi.jpg
share: false
slug: corgi-voucher
tags:
- illustration
- dogs
- animals
- voucher
- fancon
- wacom-mobile-studio-pro
- clip-studio-paint
- cv
tileImage: /tiles/digi-corgi_tile.jpg
title: Corgeh
transparent: false
comments: false
featuredTag: 
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

Little drawing I did for a promotional voucher for my Fancon2018 clients.

<figure><img src="/images/illustration/digi-corgi-vouch.jpg" data-action="zoom"/></figure>