---
date: 2017-03-03T19:04:29.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/digi-steam-dog.jpg
share: true
slug: steam-dog
tags:
- illustration
- animal-portrait
- commission
- animals
- dogs
- steampunk
tileImage: /tiles/digi-steam-dog_tile.jpg
title: Steampunk Doge
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


*Medium:* Digital, Clip Studio Paint, Wacom Intuos3

