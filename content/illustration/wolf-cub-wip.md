---
date: 2020-12-21T17:09:43+02:00
description: ""
draft: false
featured: false
tileImage: "/tiles/wolf-cub-wip-tile.jpg"
imageUrl: /images/illustration/wolf-cub/wolf-cub-sketch.jpg
imageTitle:
transparent: false
share: false
slug: wolf-cub-wip
tags:
- wip
- animals
- wolf
- in-memory-of
- wacom-mobile-studio-pro
- clip-studio-paint
title: Wolf Cub WIP
---

Trying out some colour variations. I don't usually go for such bright colours, but this was intended to be more dreamy and unreal. I also tend to like my sketches more than my paintings, but this has it's own charm.

<figure><img src="/images/illustration/wolf-cub/wolf-cub-colour.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/wolf-cub/wolf-cub-colour-shade.jpg" data-action="zoom"/></figure>