---
date: 2018-04-05T11:47:39+02:00
description: "'Puppies and Kittens!'"
draft: false
featured: false
imageTitle: null
imageUrl: /images/illustration/robertawarren.jpg
share: false
slug: roberta-warren
tags:
- illustration
- portrait
- z-nation
- zombies
- fancon
- wacom-mobile-studio pro
- clip-studio-paint
- cv
featuredTag:
  - z-nation
  - fancon
tileImage: /images/illustration/robertawarren-small.jpg
title: Roberta Warren
transparent: false
---

*Lieutenant* Roberta Warren from the National Guard

From one of my favorite zombie series, Z nation! It had a great sense of humour and character developement. How can you take a zombie apocalypse seriously? It's just too ridiculous of life (and humanity).

Done on my Wacom Mobile Studio Pro and fueled by the lack of new episodes.
