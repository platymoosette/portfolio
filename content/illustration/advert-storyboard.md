---
title: Advert Storyboards
description: 
date: 2024-05-14T19:51:49.059Z
draft: false
featured: false
tileImage: /images/set/storyboard/totem-color.jpg
imageUrl: /images/set/storyboard/totem-color.jpg
imageTitle: ''
transparent: false
share: false
slug: advert-storyboard
tags:
- cv
- storyboard
---
<figure><img src="/images/set/storyboard/totem-line.jpg" data-action="zoom" width="800"/></figure> 

<figure><img src="/images/set/storyboard/ad1scene2-a.jpg" data-action="zoom" width="1000"/></figure>

<figure><img src="/images/set/storyboard/ad2scene-b.jpg" data-action="zoom" width="1000"/></figure>

<figure><img src="/images/set/storyboard/color-hands.jpeg" data-action="zoom" width="900"/></figure>

<figure><img src="/images/set/storyboard/group-var-1.jpg" data-action="zoom" width="900"/></figure>

<figure><img src="/images/set/storyboard/group-var-2.jpg" data-action="zoom" width="900"/></figure>

<figure><img src="/images/set/storyboard/group-var-3.jpg" data-action="zoom" width="1000"/></figure>

<figure><img src="/images/set/storyboard/group-var-3-COLOR.jpg" data-action="zoom" width="1200"/></figure>
