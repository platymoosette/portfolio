---
date: 2017-03-21T19:28:43.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/platyopal-trns.png
share: true
slug: platyopal
tags:
- illustration
- platymoose
- platyfight
- fancon
- street-fighter
- comic-art
- wacom-mobile-studio-pro
- clip-studio-paint
- sticker
- steven-universe
tileImage: /tiles/platyopal-trns_tile.png
title: PlatyOpal
transparent: true
comments: false
featuredTag:
  - platyfight
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---



**Medium:** Digital, Clip Studio Paint, Wacom MobileStudio Pro



