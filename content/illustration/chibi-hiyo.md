---
date: 2017-03-03T11:19:45.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/chibi-girl.jpg
share: true
slug: chibi-girl
tags:
- illustration
- comic-art
- chibi
- gift
- cv
tileImage: /tiles/chibi-girl_tile.jpg
title: Chibi Girl
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


*Medium:* Digital, Clip Studio Paint, Wacom Intuos3

