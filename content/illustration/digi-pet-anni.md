---
date: 2018-08-03T15:15:18.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/digi-pet-anni.jpg
share: false
slug: pet-anniversary
tags:
- illustration
- gift
- commission
- anniversary
- cats
- dogs
- animals
- plants
- succulents
- cute
- cv
tileImage: /tiles/digi-pet-anni_tile.jpg
title: Pet-iversary
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


Trying out a new style with brushes to go with it, this time for another anniversary! 
Dogs, cats and succulents.