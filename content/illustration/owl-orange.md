---
date: 2017-07-23T18:42:11.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/owl-orange.jpg
share: false
slug: owl-orange
tags:
- illustration
- animals
- owl
- fancon
- wacom-mobile-studio-pro
tileImage: /tiles/owl-orange_tile.jpg
title: Paisley Owl
transparent: false
comments: false
featuredTag: 
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

Nothing like a mashup of my favorite pattern _and_ my favorite animal. Done in Photoshop on my wacom intuos 3