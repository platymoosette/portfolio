---
date: 2019-01-28T18:06:02.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/platy-skeleton.png
share: false
slug: platy-skel
tags:
- digital
- comic-art
- platymoose
- skeleton
- sticker
- wacom-mobile-studio-pro
- clip-studio-paint
- fancon
- cv
tileImage: /tiles/platy-skeleton_tile.png
title: Platy Squelette
transparent: true
comments: false
featuredTag: 
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


<figure><img src="/images/illustration/platy-skeleton1.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/platy-skeleton2.jpg" data-action="zoom"/></figure>