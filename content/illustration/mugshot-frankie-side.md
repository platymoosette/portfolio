---
date: 2017-01-24T14:20:34.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/mugshot-frankie-side.png
share: true
slug: mugshot-frankie-side
tags:
- illustration
- portrait
- mugshot
- wacom-mobile-studio-pro
- clip-studio-paint
- wip
tileImage: /tiles/mugshot-frankie-side_tile.png
title: Frankie Stein - Side
transparent: true
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Digital, Clip Studio Paint, Wacom Intuos3