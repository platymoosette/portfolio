---
date: 2017-02-25T17:53:14.0000000+02:00
description: urrrrrpppp
draft: false
featured: false
imageTitle: get schwifttyyyy
imageUrl: /images/illustration/platyrick.jpg
share: true
slug: platy-rick
tags:
- illustration
- platymoose
- platyfight
- fancon
- street-fighter
- comic-art
- wacom-mobile-studio-pro
- clip-studio-paint
- sticker
- rick-and-morty
- rick-sanchez
tileImage: /tiles/platyrick_tile.jpg
title: Platyfight Rick Sanchez
transparent: false
comments: false
featuredTag:
  - platyfight
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

Part of my PlatyFight series, featuring my mascot, the majestic Platymoose, in the street fighter pose.

**Medium:** Digital, Clip Studio Paint, Wacom Intuos 3

Cross over with Rick and Morty !


