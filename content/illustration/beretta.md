---
date: 2017-06-03T17:37:30.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: Isabella Giovani
imageUrl: /images/illustration/mugshots/beretta.jpg
share: false
slug: beretta
tags:
- illustration
- portrait
- mugshot
- fancon
- wacom-mobile-studio-pro
- clip-studio-paint
tileImage: /tiles/beretta_tile.jpg
title: Beretta
transparent: true
comments: false
featuredTag:
  - mugshot
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Digital, Clip Studio Paint, Wacom Mobile Studio Pro