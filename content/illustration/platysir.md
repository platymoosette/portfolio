---
date: 2017-03-03T19:08:45.0000000+02:00
description: the one and only
draft: false
featured: false
imageTitle: Sir Platymoose, my mascot
imageUrl: /images/illustration/platysir.jpg
share: true
slug: platysir
tags:
- illustration
- platymoose
- fancon
- comic-art
- wacom-mobile-studio-pro
- clip-studio-paint
- sticker
- cv
tileImage: /tiles/platysir_tile.jpg
title: Sir Platymoose
transparent: false
comments: false
featuredTag:
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Digital, Clip Studio Paint, Wacom Intuos 3

