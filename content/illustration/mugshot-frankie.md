---
date: 2017-01-24T15:20:34.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/mugshot-frankie.png
share: true
slug: mugshot-frankie
tags:
- illustration
- portrait
- mugshot
- wacom-mobile-studio-pro
- clip-studio-paint
- wip
tileImage: /tiles/mugshot-frankie_tile.png
title: Frankie Stein
transparent: true
comments: true
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

**Medium:** Digital, Clip Studio Paint, Wacom Intuos3