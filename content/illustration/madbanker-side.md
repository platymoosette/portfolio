---
date: 2017-06-03T17:29:23.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: Francois Aleatoire
imageUrl: /images/illustration/mugshots/madbanker-side.jpg
share: false
slug: madbanker-side
tags:
- illustration
- portrait
- mugshot
- fancon
- wacom-mobile-studio-pro
- clip-studio-paint
tileImage: /tiles/madbanker-side_tile.jpg
title: The Mad Banker
transparent: true
comments: false
featuredTag:
  - mugshot
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Digital, Clip Studio Paint, Wacom Mobile Studio Pro