---
date: 2017-01-24T12:20:34.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/mugshot-flann-side.jpg
share: true
slug: mugshot-flann-side
tags:
- illustration
- portrait
- mugshot
- wacom-mobile-studio-pro
- clip-studio-paint
- wip
tileImage: /tiles/mugshot-flann-side_tile.jpg
title: My name is Flann - Side
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


*Medium:* Digital, Clip Studio Paint, Wacom Intuos3