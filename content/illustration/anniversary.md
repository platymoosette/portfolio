---
date: 2017-06-03T17:44:35.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/digi-anni-trans.png
share: false
slug: 39anniversary
tags:
- illustration
- gift
- commission
- anniversary
- cv
tileImage: /tiles/digi-anni-trans_tile.png
title: Anniversary
transparent: true
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


A gift for my parent's 39th wedding anniversary! 

I enjoyed trawling through their scanned negatives to find their wedding photos for reference and seeing them when they were young. 

One thing I am interested in is the synthesis between photograph captured moments and painting from them. You can reflect your style but also what meaning you took from the original. What it made you feel or think and how that effected your art. What you wanted to accentuate and convey. Adding another layer of meaning. 

Done digitally in Clip Studio Paint using my Wacom Intuos 3