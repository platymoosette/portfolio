---
date: 2017-02-25T18:06:59.0000000+02:00
description: The fleshy bits
draft: false
featured: false
imageTitle: The Body
imageUrl: /images/illustration/le-corps.jpg
share: true
slug: le-corps
tags:
- illustration
- commission
- vector
- educational
- french
- adventure-time
- cv
- wacom-mobile-studio-pro
- clip-studio-paint
tileImage: /tiles/le-corps_tile.jpg
title: Le Corps
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

A series of educational posters to help learn the body parts en français!

*Medium:* Digital (vector), Clip Studio Paint, Wacom Intuos3
