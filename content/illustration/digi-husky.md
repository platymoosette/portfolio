---
date: 2017-02-25T19:00:58.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/digi-husky.jpg
share: true
slug: husky
tags:
- digital-painting
- animal-portrait
- dogs
- animals
tileImage: /tiles/digi-husky_tile.jpg
title: Husky
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


*Medium:* Digital, Adobe Photoshop, Wacom Intuos3