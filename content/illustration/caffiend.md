---
date: 2020-12-20T17:05:43+02:00
description: ""
draft: false
featured: false
tileImage: "/tiles/caffiend-trans-tile.png"
imageUrl: /images/illustration/caffiend.jpg
imageTitle:
transparent: true
share: false
slug: caffiend
tags:
- illustration
- doodle
- ipad-pro
- procreate
title: Caffiend
---

<figure><img src="/images/illustration/caffiend-sketch.jpg" data-action="zoom"/></figure>