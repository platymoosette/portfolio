---
date: 2017-06-03T17:38:51.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: Sofya Semyonovna
imageUrl: /images/illustration/mugshots/cleaner.jpg
share: false
slug: cleaner
tags:
- illustration
- portrait
- mugshot
- fancon
- wacom-mobile-studio-pro
- clip-studio-paint
- cv
tileImage: /tiles/cleaner_tile.jpg
title: The Cleaner
transparent: true
comments: false
featuredTag:
  - mugshot
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Digital, Clip Studio Paint, Wacom Mobile Studio Pro