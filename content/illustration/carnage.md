---
date: 2017-06-03T17:38:02.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: Jonny Kolnikov
imageUrl: /images/illustration/mugshots/carnage.jpg
share: false
slug: carnage
tags:
- illustration
- portrait
- mugshot
- fancon
- wacom-mobile-studio-pro
- clip-studio-paint
tileImage: /tiles/carnage_tile.jpg
title: Carnage
transparent: true
comments: false
featuredTag:
  - mugshot
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Digital, Clip Studio Paint, Wacom Mobile Studio Pro