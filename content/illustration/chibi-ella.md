---
date: 2017-03-03T11:19:41.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/chibi-pug.jpg
share: true
slug: chibi-pug
tags:
- illustration
- comic-art
- chibi
- gift
- animals
- cv
tileImage: /tiles/chibi-pug_tile.jpg
title: Chibi Pug
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Digital, Clip Studio Paint, Wacom Intuos3