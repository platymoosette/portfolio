---
title: Garden Cub
description: 
date: 2024-05-14T15:04:04.063Z
draft: false
featured: true
tileImage: /tiles/garden-cub_tile.jpg
imageUrl: /images/painting/garden-cub.jpg
imageTitle: charcoal pupper
transparent: false
share: false
slug: garden-cub
tags:
- cv
---
Some rendering progress of my little wolf cub. Half memorial, half idea exploration, it's become one of my favourites to render in different styles while I figure out what I want from it.

[other pupper wip](/illustration/wolf-cub-wip)