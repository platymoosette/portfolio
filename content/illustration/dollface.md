---
date: 2017-06-03T17:39:35.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: Alma Prio
imageUrl: /images/illustration/mugshots/dollface.jpg
share: false
slug: dollface
tags:
- illustration
- portrait
- mugshot
- fancon
- wacom-mobile-studio-pro
- clip-studio-paint
- cv
tileImage: /tiles/dollface_tile.jpg
title: Doll Face
transparent: true
comments: false
featuredTag:
  - mugshot
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Digital, Clip Studio Paint, Wacom Mobile Studio Pro