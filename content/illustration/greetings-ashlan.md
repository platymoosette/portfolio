---
date: 2018-08-03T11:51:01.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/greetingcard-ashlan.jpg
share: false
slug: merry-catmas
tags:
- illustration
- greeting-card
- gift
- cats
- animals
- cute
- catmas
- holidays
- cv
tileImage: /tiles/greetingcard-ashlan_tile.jpg
title: Merry Catmas
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

Just what you need on a special holiday, cat bite fever

<figure><img src="/images/illustration/ashlan idea.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/ashlan take1.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/ashlan transpwater.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/ashlan transpwater it noline.jpg" data-action="zoom"/></figure>