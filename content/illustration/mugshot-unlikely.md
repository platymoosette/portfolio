---
date: 2017-01-24T19:20:34.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/mugshot-unlikely.png
share: true
slug: mugshot-unlikely
tags:
- illustration
- portrait
- mugshot
- wacom-mobile-studio-pro
- clip-studio-paint
- retro
- wip
tileImage: /tiles/mugshot-unlikely_tile.png
title: Unlikely suspect
transparent: true
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---



**Medium:** Digital, Clip Studio Paint, Wacom Intuos3
