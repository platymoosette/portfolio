---
date: 2017-02-25T18:06:58+02:00
description: "The squishy bits"
draft: false
featured: false
imageTitle: The Organs
imageUrl: /images/illustration/les-organes.jpg.
tileImage: /images/illustration/les-organes.jpg
share: true
slug: les-organes
tags:
- illustration
- commission
- vector
- educational
- french
- adventure-time
- cv
- wacom-mobile-studio-pro
- clip-studio-paint
title: Les Organes
transparent: false
---
A series of educational posters to help learn the body parts en français!

*Medium:* Digital (vector), Clip Studio Paint, Wacom Intuos3

