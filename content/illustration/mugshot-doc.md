---
date: 2017-01-24T09:20:34.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/mugshot-doc.png
share: true
slug: mugshot-doc
tags:
- illustration
- portrait
- mugshot
- retro
- wacom-mobile-studio-pro
- clip-studio-paint
- wip
tileImage: /tiles/mugshot-doc_tile.png
title: Doc
transparent: true
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Digital, Clip Studio Paint, Wacom Intuos3