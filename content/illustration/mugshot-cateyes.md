---
date: 2017-01-24T18:20:34.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: Sassy mcSassyFace
imageUrl: /images/illustration/mugshot-cateyes.png
share: true
slug: mugshot-cateyes
tags:
- illustration
- portrait
- mugshot
- retro
- wacom-mobile-studio-pro
- clip-studio-paint
- wip
tileImage: /tiles/mugshot-cateyes_tile.png
title: Cat Eyes
transparent: true
comments: false
featuredTag:
  - mugshot
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Digital, Clip Studio Paint, Wacom Intuos3