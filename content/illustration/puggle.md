---
date: 2018-04-25T23:24:49.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/puggle.png
share: false
slug: pillow-puggle
tags:
- illustration
- sticker
- fancon
- dogs
- animals
- pug
- cute
- wacom-mobile-studio-pro
- clip-studio-paint
- cv
tileImage: /tiles/puggle_tile.png
title: Pillow puggle
transparent: true
comments: false
featuredTag:
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


Another little drawing I did for Fancon2018. Look at the squidge!