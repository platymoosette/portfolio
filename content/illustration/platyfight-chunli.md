---
date: 2017-03-14T01:46:46.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/platychunli-trns.png
share: true
slug: platyfight-chunli
tags:
- illustration
- platymoose
- platyfight
- fancon
- street-fighter
- comic-art
- wacom-mobile-studio-pro
- clip-studio-paint
- sticker
tileImage: /tiles/platychunli-trns_tile.png
title: Chunli
transparent: true
comments: false
featuredTag:
  - platyfight
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


Part of my PlatyFight series featuring my mascot, the majestic Platymoose in some variety of street fighter get up.

**Medium:** Digital, Clip Studio Paint, Wacom MobileStudio Pro