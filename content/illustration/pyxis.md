---
date: 2020-12-19T13:08:43+02:00
description: ""
draft: false
featured: false
tileImage: "/tiles/pyxis-tile-crop.jpg"
imageUrl: /images/illustration/pyxis.jpg
imageTitle:
transparent: false
share: false
slug: pyxis
tags:
- illustration
- dogs
- animals
- ipad-pro
- procreate
- commission
- tattoo
title: Pyxis
---

This was a quick sketch for a sweet doge as a little surprise 
after I was asked to draw a tattoo in commemoration of her.

Some days I want to test new brushes and need reference material, which this time happily coincided with wanting to try sketching this chinese crested. 

Her nickname was pickle, which inspired the tattoo concept.

Here were some of the ideas I was working through.

<figure><img src="/images/illustration/pickle-ideas.jpg" data-action="zoom"/></figure>