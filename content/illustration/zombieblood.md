---
date: 2018-04-05T13:56:58.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/zombieblood.png
share: false
slug: zombie-blood
tags:
- illustration
- sticker
- fancon
- zombies
- RPG
- potion
- wacom-mobile-studio-pro
- clip-studio-paint
- cv
tileImage: /tiles/zombieblood_tile.png
title: Zombie Blood Potion
transparent: true
comments: false
featuredTag: 
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


I wanted to paint a few things specifically to print as stickers for Fancon this year. This one was inspired by Health and Mana Potions, and really pining for zombie themed merch!

