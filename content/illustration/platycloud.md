---
date: 2019-01-10T00:40:19.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/platycloud-trns copy.png
share: false
slug: platy-cloud
tags:
- illustration
- platymoose
- platyfight
- fancon
- street-fighter
- comic-art
- wacom-mobile-studio-pro
- clip-studio-paint
- final-fantasy
- cloud
- buster-sword
- sticker
tileImage: /tiles/platycloud-trns copy_tile.png
title: Platy Cloud
transparent: true
comments: false
featuredTag:
  - platyfight
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


A platymoose street fighter cross over with Final Fantasy 7 character, Cloud. One of the best FF games I've played. The buster sword was my favorite! 

<img src="/images/illustration/cloud.png">