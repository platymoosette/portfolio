---
date: 2019-01-10T00:42:19+02:00
description: ""
draft: false
featured: false
imageTitle: null
imageUrl: /images/illustration/barberian/barbarian-logo-simple.png
share: false
slug: barber-ian
tags:
- illustration
- design
- commission
- merchandise
- barnet-far-barber-shop
- logo
- barber
- vector
- wacom-mobile-studio-pro
- clip-studio-paint
- cv
tileImage: /images/illustration/barberian/barber-ian.gif
title: Barber-ian character
transparent: true
---
Large and small character design for Barnet Fairs coffee shop logo.

<figure><img src="/images/illustration/barberian/barberian-logo-big.png" data-action="zoom"/></figure>



