---
date: 2017-02-25T19:00:57.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/digi-husky-run.jpg
share: true
slug: digi-husky-run
tags:
- digital-painting
- dogs
- figure-study
- animals
tileImage: /tiles/digi-husky-run_tile.jpg
title: Huskies Running
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


*Medium:* Digital, Adobe Photoshop, Wacom Intuos3