---
date: 2019-01-28T15:44:29+02:00
description: "A little practice image playing around with form and shading, still in progress"
draft: false
featured: false
imageTitle: null
imageUrl: /images/illustration/owlgirl/
share: false
slug: owl-girl
tags:
- wip
- owl
- animals
- wacom-mobile-studio-pro
- clip-studio-paint
tileImage: /images/illustration/owlgirl/owlgirl8.jpg
title: Owl Girl
transparent: false
---

<figure><img src="/images/illustration/owlgirl/owlgirlline.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/owlgirl/owlgirl8.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/owlgirl/owlgirl2_02.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/owlgirl/owlgirl2_0.jpg" data-action="zoom"/></figure>

