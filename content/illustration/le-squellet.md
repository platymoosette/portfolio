---
date: 2017-02-25T18:06:57.0000000+02:00
description: the strong bits
draft: false
featured: false
imageTitle: The Skeleton
imageUrl: /images/illustration/le-squellet.jpg
share: true
slug: le-squellet
tags:
- illustration
- commission
- vector
- educational
- french
- adventure-time
- cv
- wacom-mobile-studio-pro
- clip-studio-paint
tileImage: /tiles/le-squellet_tile.jpg
title: Le Squellet
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

A series of educational posters to help learn the body parts en français!

*Medium:* Digital (vector), Clip Studio Paint, Wacom Intuos3
