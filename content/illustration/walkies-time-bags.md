---
date: 2020-12-19T14:09:43+02:00
description: ""
draft: false
featured: false
tileImage: "/tiles/walkies-time-tile.jpg"
imageUrl: /images/illustration/walkies-time.jpg
imageTitle:
transparent: false
share: false
slug: walkies-time
tags:
- commission
- illustration
- storyboard
- poop
- wacom-mobile-studio-pro
- clip-studio-paint
- cv
title: Walkies Time Bag Poster
---

A poster drawn to illustrate the use of little bags to pick up your dog's poop :>
not the finished poster as there was someone else doing the text.

<figure><img src="/images/illustration/walkies-time-panel1.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/walkies-time-panel2.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/walkies-time-panel3.jpg" data-action="zoom"/></figure>

The idea sketch

<figure><img src="/images/illustration/walkies-time-sketch.jpg" data-action="zoom"/></figure>