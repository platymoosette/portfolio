---
date: 2017-01-24T10:20:34.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/mugshot-doekie.jpg
share: true
slug: mugshot-dont-mess
tags:
- illustration
- portrait
- mugshot
- wacom-mobile-studio-pro
- clip-studio-paint
- retro
- wip
tileImage: /tiles/mugshot-doekie_tile.jpg
title: Don't Mess
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


*Medium:* Digital, Clip Studio Paint, Wacom Intuos3