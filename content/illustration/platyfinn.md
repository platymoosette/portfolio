---
date: 2017-02-25T18:00:45+02:00
description: "mathematical!"
draft: false
featured: false
tileImage: /images/illustration/platyfinn.jpg
imageUrl: /images/illustration//platyfinn.jpg
imageTitle:
transparent: false
share: true
slug: platyfinn
tags:
- illustration
- platymoose
- platyfight
- fancon
- street-fighter
- comic-art
- wacom-mobile-studio-pro
- clip-studio-paint
- adventure-time
- sticker
title: Platyfight Finn
featuredTag:
  - platyfight
  - fancon
---
Part of my PlatyFight series, featuring my mascot, the majestic Platymoose, in the street fighter pose.

**Medium:** Digital, Clip Studio Paint, Wacom Intuos 3

Cross over with Adventure Time !