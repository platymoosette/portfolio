---
date: 2017-02-25T17:53:21.0000000+02:00
description: I thought you were a princess!
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/platyking.jpg
share: true
slug: platyking
tags:
- illustration
- platymoose
- platyfight
- street-fighter
- comic-art
- wacom-mobile-studio-pro
- clip-studio-paint
- adventure-time
tileImage: /tiles/platyking_tile.jpg
title: Platyfight Ice king
transparent: false
comments: false
featuredTag:
  - platyfight
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

Part of my PlatyFight series, featuring my mascot, the majestic Platymoose, in the street fighter pose.

**Medium:** Digital, Clip Studio Paint, Wacom Intuos 3

Cross over with Adventure Time !
