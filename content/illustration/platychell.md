---
date: 2019-01-10T00:41:19.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/platychell-trns.png
share: false
slug: platy-chell
tags:
- illustration
- platymoose
- platyfight
- fancon
- street-fighter
- comic-art
- wacom-mobile-studio-pro
- clip-studio-paint
- valve
- portal
- portal-gun
- chell
- sticker
tileImage: /tiles/platychell-trns_tile.png
title: Platy Chell
transparent: true
comments: false
featuredTag:
  - platyfight
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


Another one of my street fighter cross overs, this time with a character from one of my favorite puzzle games, portal

<img src="/images/illustration/chell2.jpg">