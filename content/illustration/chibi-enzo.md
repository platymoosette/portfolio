---
date: 2017-03-03T11:19:36.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/chibi-guy.jpg
share: true
slug: chibi-guy
tags:
- illustration
- comic-art
- chibi
- gift
- cv
tileImage: /tiles/chibi-guy_tile.jpg
title: Chibi Guy
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---



**Medium:** Digital, Adobe Photoshop, Wacom Intuos3

