---
date: 2017-02-24T20:20:34.0000000+02:00
description: '* pew pew *'
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/platypixel.jpg
share: true
slug: platypixel
tags:
- illustration
- platymoose
- platyfight
- street-fighter
- comic-art
- wacom-mobile-studio-pro
- clip-studio-paint
tileImage: /tiles/platypixel_tile.jpg
title: Platypixel
transparent: false
comments: false
featuredTag:
  - platyfight
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

Part of my PlatyFight series, featuring my mascot, the majestic Platymoose, in the street fighter pose. This time retro pixel style

**Medium:** Digital, Clip Studio Paint, Wacom Intuos 3


