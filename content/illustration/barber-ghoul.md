---
date: 2019-01-10T00:42:32+02:00
description: ""
draft: false
featured: false
imageTitle: null
imageUrl: /images/illustration/barberghoul/barberghoul-fin-black.jpg
share: false
slug: barber-ghoul
tags:
- illustration
- commission
- t-shirt-design
- merchandise
- barber
- ghoul
- skull
- barnet-fair-barber-shop
- cv    
tileImage: /images/illustration/barberghoul/barberghoul-tile.jpg
title: Barber Ghoul
transparent: false
---
Something I came up with for a really cool Barber shop called [Barnet Fair](https://www.facebook.com/Barnet-Fair-Barber-shop-283549548856759/) that wanted an edgey, artist's tongue in cheek take on a barber for their new tshirt range. Ghoulish, Sweeney todd vibes and a love for tattoos and anything vintage or gothic. Elements that aligned with their own brand esthetic.
They were really great in giving me some keywords and pictures for inspiration and then letting me go with what I was inspired to create.


Some closeups
<figure><img src="/images/illustration/barberghoul/barberghoul-face.jpg" data-action="zoom"/><figcaption></figcaption></figure>

<figure><img src="/images/illustration/barberghoul/barberghoul-skull.jpg" data-action="zoom"/><figcaption></figcaption></figure>

<figure><img src="/images/illustration/barberghoul/barberghoul-dress.jpg" data-action="zoom"/><figcaption></figcaption></figure>

Here are some snapshots from the design process.
First some ideas I sketched up after listening to their criteria and looking at their inspiration pictures.

<figure><img src="/images/illustration/barberghoul/barberghoul-idea1.jpg" data-action="zoom"/></figure>
<figure><img src="/images/illustration/barberghoul/barberghoul-idea2.jpg" data-action="zoom"/></figure>

After sending them my sketches, they chose an idea and I started fleshing it out a bit more.

<figure><img src="/images/illustration/barberghoul/barberghoul-comp1.jpg" data-action="zoom"/></figure>

And adjusting some design elements
<figure><img src="/images/illustration/barberghoul/barberghoul-comp2.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/barberghoul/barberghoul-comp3.jpg" data-action="zoom"/></figure>

Heres a version in white. They really liked the idea of blood splatter in the background

<figure><img src="/images/illustration/barberghoul/barberghoul-fin-white.jpg" data-action="zoom"/></figure> 


Check them out on their website, [http://www.barnetfair.co.za/](http://www.barnetfair.co.za/), their [facebook](https://www.facebook.com/Barnet-Fair-Barber-shop-283549548856759/) or [instagram](https://www.instagram.com/barnetfairbarber_cpt/)

