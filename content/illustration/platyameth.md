---
date: 2017-03-18T18:26:51.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/platyameth-trns.png
share: true
slug: platyameth
tags:
- illustration
- platymoose
- platyfight
- fancon
- street-fighter
- comic-art
- wacom-mobile-studio-pro
- clip-studio-paint
- steven-universe
- sticker
tileImage: /tiles/platyameth-trns_tile.png
title: PlatyAmethyst
transparent: true
comments: true
featuredTag:
  - platyfight
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---



**Medium:** Digital, Clip Studio Paint, Wacom MobileStudio Pro
