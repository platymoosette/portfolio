---
date: 2020-12-20T17:09:43+02:00
description: "A rebel in a short body"
draft: false
featured: false
tileImage: "/tiles/pastel-pugu-circle-tile2.png"
imageUrl: /images/illustration/pastel-pugu-small.jpg
imageTitle: null
transparent: true
share: false
slug: pastel-hell-pug
tags:
- illustration
- merchandise
- dogs
- animals
- cute
- wacom mobile studio pro
- clip studio paint
- redbubble
- cv
title: Pastel Hell Pug
---
Available on my [redbubble](https://www.redbubble.com/people/platymoosette/shop?asc=u) in some creatively weird products.

![cute but fiery](/images/illustration/pastel-pug-redbubble-pouch.jpg)
