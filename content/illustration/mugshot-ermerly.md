---
date: 2017-01-24T11:20:34.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/mugshot-ermerly.jpg
share: true
slug: mugshot-ermerly
tags:
- illustration
- portrait
- mugshot
- wacom-mobile-studio-pro
- clip-studio-paint
- wip
tileImage: /tiles/mugshot-ermerly_tile.jpg
title: O'mally
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


*Medium:* Digital, Clip Studio Paint, Wacom Intuos3

