---
featuredTag:
  - memories
title: Memories - Tarot
description: ''
tags:
  - illustration
  - video
  - memories
  - in-memory-of
  - ipad-pro
  - procreate
  - tarot
  - symbology
date: 2020-12-26T15:09:43.000Z
draft: false
featured: false
tileImage: /tiles/memories-tarot-tile2.jpg
imageUrl: /images/illustration/memories/memories-tarot.jpg
imageTitle: null
transparent: false
share: false
slug: memories-tarot
---

I still have your decks, still have your incense, saving it up for days to remember you.

<video id=""
 src="/images/illustration/memories/memories-tarot-vid.mov"
 width="" height=""
 preload="auto" 
 autoplay loop controls 
 style="max-width: 100%; width: auto !important;height: auto !important;"
 poster="/tiles/memories-tarot-tile.jpg" title="Tap to play/pause"> </video>
