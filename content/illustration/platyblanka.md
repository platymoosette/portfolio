---
date: 2017-02-25T17:53:13.0000000+02:00
description: Now the beast is unleashed!
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/platyblanka.jpg
share: true
slug: platyblanka
tags:
- illustration
- platymoose
- platyfight
- fancon
- street-fighter
- comic-art
- wacom-mobile-studio-pro
- clip-studio-paint
tileImage: /tiles/platyblanka_tile.jpg
title: Platyfight Blanka
transparent: false
comments: false
featuredTag:
  - platyfight
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

Part of my PlatyFight series, featuring my mascot, the majestic Platymoose, in the street fighter pose.

**Medium:** Digital, Clip Studio Paint, Wacom Intuos 3

"Whroo-oooooooooooooooo!"