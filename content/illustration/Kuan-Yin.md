---
title: Kuan Yin
description: her steps remain colorful memories
date: 2024-05-14T20:19:00.070Z
draft: false
featured: true
tileImage: /tiles/Ky-toes-trns_tile.png
imageUrl: /ky-toes-chrom.jpg
transparent: true
share: false
slug: ky-toes
tags:
- cv
---

A memorial piece for the closest soul-mate I've had the joy of loving.\
 \
I thought I'd get to enjoy life with her for a bit longer, but the beautiful parts of life are the things you aren't in control of, things like time, life, death. They take and give things you could never imagine. Kuan Yin was part of that beauty, the natural state of life, she was her own being. I couldn't control her or how she enjoyed her time on this earth. She hunted, explored, played, fought and loved with 110% commitment, she did things that astounded me every day. \
And that's a precious thing, even if parts of me still want what I couldn't have. I feel so lucky to have known a soul as wild and joyous as Kuan Yin's. She lived up to her name-sake, and she's left a deep connection etched in my soul.\
\
Don't rest in peace, rest in grace, in passion, in love, in mad-cap games around an endless garden. Rest doing what you loved best. Don't hold back for anything.
