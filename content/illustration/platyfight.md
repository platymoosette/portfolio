---
date: 2017-02-25T15:48:23.0000000+02:00
description: standing animation coming soon to a browser near you!
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/platyfight.jpg
share: true
slug: platyfight
tags:
- illustration
- platymoose
- platyfight
- street-fighter
- comic-art
- wacom-mobile-studio-pro
- clip-studio-paint
tileImage: /tiles/platyfight_tile.jpg
title: Platymoose Fight!
transparent: false
comments: false
featuredTag:
  - platyfight
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---

Part of my PlatyFight series, featuring my mascot, the majestic Platymoose, in the street fighter pose. The original.

**Medium:** Digital, Clip Studio Paint, Wacom Intuos 3

