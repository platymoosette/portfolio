---
date: 2018-08-03T11:50:32.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/illustration/greetingcard-daisy.jpg
share: false
slug: merry-doggo
tags:
- illustration
- greeting-card
- gift
- dogs
- animals
- cute
- dogmas
- holidays
- cv
tileImage: /tiles/greetingcard-daisy_tile.jpg
title: Merry Doggo
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


Twinkly lights and little dog bites, these are a few of my favorite things !

<figure><img src="/images/illustration/daisy-sketch.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/daisy1.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/daisy2.jpg" data-action="zoom"/></figure>

<figure><img src="/images/illustration/daisy3.jpg" data-action="zoom"/></figure>