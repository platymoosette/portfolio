---
date: 2017-06-03T17:39:21.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: Gina Mancini
imageUrl: /images/illustration/mugshots/mayhem.jpg
share: false
slug: mayhem
tags:
- illustration
- portrait
- mugshot
- fancon
- wacom-mobile-studio-pro
- clip-studio-paint
tileImage: /tiles/mayhem_tile.jpg
title: Mayhem
transparent: true
comments: false
featuredTag:
  - mugshot
  - fancon
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


**Medium:** Digital, Clip Studio Paint, Wacom Mobile Studio Pro