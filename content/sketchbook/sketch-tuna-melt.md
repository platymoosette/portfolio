---
date: 2017-02-27T18:17:12.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/sketchbook/sketch-tuna-melt.jpg
share: true
slug: tuna-melt
tags:
- pencil-drawing
- animals
- dogs
- tuna
- chiweenie
tileImage: /tiles/sketch-tuna-melt_tile.jpg
title: Tuna Melt
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


He's real, he's a chiweenie, and he has an [instagram](https://www.instagram.com/tunameltsmyheart/) !


