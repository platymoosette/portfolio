---
date: 2019-01-09T13:19:09.0000000+02:00
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: /images/sketchbook/sketch-genna.jpg
share: false
slug: gemma
tags:
- sketch
- pencil-drawing
- fantasy
- character-art
tileImage: /tiles/sketch-genna_tile.jpg
title: Genna - character
transparent: false
comments: false
featuredTag: 
featuredTray: false
unlisted: false
showGalleryTitles: false
author: 
gallery: 
---


A sketch of a character from my graphic novel still in the works