---
date: 2020-12-20T15:09:43+02:00
description: ""
draft: false
featured: false
tileImage: "/tiles/portrait-prac-lecicatrise-tile2.jpg"
imageUrl: /images/sketchbook/portrait-prac-lecicatrise.jpg
imageTitle:
transparent: false
share: false
slug: portrait-prac-lecicatrise
tags:
- ipad-pro
- procreate
- video
- wip
- digital-drawing
- portrait
title: Portrait Practice - le Cicatrise
---

<video id=""
    src="/images/sketchbook/portrait-prac.mp4"
    width="" height=""
    preload="auto" 
    autoplay loop controls 
    style="max-width: 100%; width: auto !important;height: auto !important;"
    poster="/images/portrait-prac-lecicatrise-tile.jpg" title="Tap to play/pause">
</video>