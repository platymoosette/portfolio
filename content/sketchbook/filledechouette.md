---
date: 2017-09-15T14:43:11+02:00
description: "One of my first process videos"
draft: false
featured: false
imageTitle: null
imageUrl: 
share: true
slug: fille-de-chouette
tags:
- pen-drawing
- video
- wip
- owl
- animals
tileImage: /tiles/chouette-tile2.jpg
title: Fille de chouette
transparent: false
---

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/b06289d96401468b835b2e3298e7d462?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

I really wanted to start recording my drawing process and to be able to stream online, so this was my first test for camera set up and video editing. Testing out a new brush pen and loosening up my rusty claw grip after months of digital work. WHAT IS PEN

<figure><img src="/images/sketchbook/chouettefille.jpg" data-action="zoom"/></figure>

<figure><img src="/images/sketchbook/sketch-chouettefille.jpg" data-action="zoom"/></figure>