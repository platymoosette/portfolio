---
date: 2020-12-20T19:09:43+02:00
description: ""
draft: false
featured: false
tileImage: "/tiles/justice-tarot-tile2.jpg"
imageUrl: /images/sketchbook/justice-tarot.jpg
imageTitle:
transparent: false
share: false
slug: justice-tarot-wip
tags:
- wip
- ipad-pro
- procreate
- video
- tarot
- symbology
title: Justice Tarot WIP
---
Working on some new themes, like tarot cards.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/88bfea2b18a542778cd78cc983aa210f?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>
