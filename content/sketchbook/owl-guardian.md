		---
date: 2017-10-05T13:28:11+02:00
description: "Another process video"
draft: false
featured: false
imageTitle: null
imageUrl: 
share: false
slug: owl-guardian
tags:
- pen-drawing
- video
- wip
- owl
- animals		
tileImage: /tiles/owl-guardian-tile2.jpg
title: Owl Guardian
transparent: false
---

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/b48a6e9729234ffbb8b11c850fbcc045?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Guardian or grim reaper? She didn't know. All she could feel was the ice cold seeping into her bones, chilling her breathing, and the whisper of feathers...

<figure><img src="/images/sketchbook/owl-guardian.jpg" data-action="zoom"/></figure>