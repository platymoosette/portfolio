---
menu: custom
slug: aboutme
title: "Who dat?"
---
<p style="text-align: center;">I do stuff! Look at me, doing stuff</p>

![hard at work](/images/services/platymoose-header2.png)

<p style="text-align: center;">Animal stuff</p>

![animals everywhere](/images/services/animal-portrait-example.png)

<p style="text-align: center;">People stuff</p>

![love dem mugs](/images/services/illustrated-portraits.png)

<p style="text-align: center;">Painting stuff</p>

![swoosh](/images/services/digital-portraits.png)

<p style="text-align: center;">Look at that wow</p>

![comemorative dementia](/images/services/digital-anniversary.png)

<p style="text-align: center;">Pencils be rocking</p>

![pencil stuff](/images/services/drawn-anniversary.png)

<p style="text-align: center;">Even stuff because of reasons</p>

![even at holidays](/images/services/greetingcards.png)

<p style="text-align: center;">Yep that's me doing stuff.</p>
