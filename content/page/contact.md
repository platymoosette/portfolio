---
menu: "custom"
slug: contact-me
title: Contact-me
---

While we get things up and running, please email rene @ platymoose.ninja (or you can just say Hi below!)

You can also subscribe to my newsletter below for updates and random platymeese.
<form style="padding:3px" action="https://tinyletter.com/platymoose" method="post" target="popupwindow" onsubmit="window.open('https://tinyletter.com/platymoose', 'popupwindow', 'scrollbars=yes,width=800,height=600');return true">
<p>
<label for="tlemail">Enter your email address:</label>
</p
><p>
<input type="text" style="width:25em" name="email" id="tlemail" />
</p>
<input type="hidden" value="1" name="embed"/>
<input type="submit" value="Subscribe" />
</form>