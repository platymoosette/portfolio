---
date: 2017-07-26 12:43:24 +0000
description: ''
draft: false
featured: false
imageTitle: 
imageUrl: "//"
share: false
slug: services
tags:
- 
tileImage: "/images/"
title: List of Services
transparent: false

---

![platybean says hi](/images/services/platy-bean-transp-small.png)

I paint digital portraits of animals
[![animal-portraits](/images/services/animal-portrait-example.png)](https://platymoose.ninja/tags/digital-animal-portrait)


and of people
[![painted-portraits](/images/services/digital-portraits.png)](https://platymoose.ninja/tags/painted-portrait/)

I love portraiture in general 
[![illustrated-portraits](/images/services/illustrated-portraits.png)](https://platymoose.ninja/tags/portrait/)


I also illustrate personalised greeting cards
[![greeting-cards](/images/services/greetingcards.png)](https://platymoose.ninja/tags/greeting-card/)

and cards or drawings for special moments like birthdays and anniversaries
[![digital-anniversaries](/images/services/digital-anniversary.png)](https://platymoose.ninja/tags/anniversary/)
[![hand-drawn-anniversaries](/images/services/drawn-anniversary.png)](https://platymoose.ninja/tags/anniversary/)

And I do a whole lot of design work for [merchandise](https://platymoose.ninja/tags/merchandise), illustrate [storyboards](https://platymoose.ninja/tags/storyboard), design characters, [illustrate for books](https://platymoose.ninja/set/comic-book/) and a whole bunch more!

So please [send me an email](https://platymoose.ninja/page/contact-me/) or Say Hi in the chat box below to ask about commissioning something.


![platymoose at work](/images/services/platymoose-header2.png)
