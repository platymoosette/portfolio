module.exports = {
    staticFileGlobs: [
        // 'css/**.css',
        'js/main.js',
        'css/bl*.css',
        'fonts/rale*.woff2',
        'fonts/noto*.woff2',
        // 'img/**.*',
        '**/*.html'
    ],
    runtimeCaching: [{
        urlPattern: /[.](png|jpg|gif|mp4|ttf|woff2|html|css|js)$/,
        handler: 'fastest'
    }]
};
