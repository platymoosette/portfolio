import * as Swiper from 'swiper';
import 'swiper';
import 'barba.js';
import * as Barba from '../../node_modules/barba.js/dist/barba.js';

import {ready2} from './entry.es6';
import './cf-beacon'

function viewChanged() {
    try {
        window.scrollTo(0, 0);
        let t = document.querySelectorAll('img[data-action="zoom"]');
        for (let n = 0; n < t.length; n++)
            window['z'].a.setup(t[n]);
    } catch (e) {
    }
    try {
        window["listenImages"]();
        makeGallery();
    } catch (e) {
    }
}

function makeGallery() {
    window.galleries = {};
    let galTop = window.galleries['top' + document.location.pathname] = new window.Swiper.default('.gallery-top', {
        zoom: {
            enabled: true,
            toggle: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        scrollbar: {
            el: '.swiper-scrollbar',
        },
        keyboard: {
            enabled: true
        }
    });
    let galThumbs = window.galleries['thumbs' + document.location.pathname] = new window.Swiper.default('.gallery-thumbs', {
        spaceBetween: 10,
        centeredSlides: true,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        slideToClickedSlide: true
    });
    if (galTop.controller != null) {
        galTop.controller.control = galThumbs
        galThumbs.controller.control = galTop
    }
}

window.addEventListener("load", makeGallery);

function initPjax() {
    Barba.Pjax.Dom.wrapperId = 'pj-wrap';
    Barba.Pjax.Dom.containerClass = 'pj-container';

    Barba.Pjax.start();
    Barba.Prefetch.init();
    Barba.transitionLength = 1000;
    Barba.Dispatcher.on('transitionCompleted', viewChanged);
    // window.BTX = BTX;
    Barba.Dispatcher.on('linkClicked', () => document.getElementById('menu-switch')['checked'] = false);
    window["Swiper"] = Swiper;
    window["Barba"] = Barba;
}

if (document.location.host.indexOf('.') > 0 && document.location.protocol == 'http:') {
    document.location.protocol = 'https:';
}

document.addEventListener("DOMContentLoaded", initPjax);

// Object.assign(window, {tryContact: tryContact});
// window['tryContact'] = tryContact;
window['Swiper'] = Swiper;
document.documentElement.className += ' loaded';
document.documentElement.className += ' hasscript';
document.documentElement.className = document.documentElement.className.replace('noscript', '');
// window.tryContact = tryContact;
ready2();