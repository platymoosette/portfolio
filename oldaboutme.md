You have stumbled across the portfolio of yours truly, Rene von Raptor. An artist, a creator, a kind weirdo, and secretly an ancient reptile. Welcome to my treasure trove.

**Who am I even?**

A whirligig of inspiration! I love portraiture, the human face and the play of light on skin is incredibly beautiful to me. Animals are secretly my favorite subject matter, so expressive and quirky. I like the bizarre, the interesting, the candid, the weird, the things that confuse you enough to look again. I am both complex and whimsical, and cant help but represent that in my art. 

**What do I make?**

Well, I work in a variety of mediums, being a real dinosaur of all trades. During school I was trained in traditional mediums like pencil, acrylics, chalk pastel and ceramics, followed by my own exploration into sculpture, sewing, digital art and animation. I’ve covered a large swathe of skills and interests and found true joy in learning whatever is close enough to express my imagination and weirdnesses. I mostly work digitally now.

**Do I do commissions?**

Most certainly. Right now I’ve been getting portraiture commissions, both of people and animals. Capturing expression is a fascinating challenge. 

I illustrate for the web, for tshirts, stickers and informational posters.
I’ve also been making original bags based on forest animals, and animal and pop culture inspired aprons, ranging from lumo fabrics to soft felts and interesting pattern combinations.

I've even been doing some storyboarding for adverts and animations.

**Where do I even?**

I’ve been trying to push myself to exhibit and show my art, going to local markets and events and promoting myself online. My most recent adventure was being an exhibitor in the Artists Alley at [Fancon](https://www.fancon.co.za/) 2017 in Cape Town, and I'll be there again this year on the 28th-29th of April at the CTICC.

You can find me via my _[contact page](/page/contact)_ and ask me about prices and commissions, or on _[my facebook page](https://www.facebook.com/theplatymoose/)_, where I sell digital prints and also courier handmade items to you. Otherwise I'm making the transition to online stores like redbubble and society6 which will soon be available.

I think thats about it, I hope you enjoy perusing my strange museum !