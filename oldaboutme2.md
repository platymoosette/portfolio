---
menu: custom
slug: aboutme
title: "Who dat?"
---

Greetings, earthlings!

In case you know nothing about me, this is my portfolio, full of my best pieces of work and weirdest brain farts.

I'm a whirligig of inspiration and have had years of experimenting with as many media as possible, after having been born with a paintbrush in paw. Most of the time I love portraiture and capturing people. The rest of the time I paint and sketch animals, zombies and anything bizzare or quirky I find in my brain.

While those are my personal favourites, I really am a creature of all trades, working in many different styles and media, both traditional and digital. I've illustrated for books, for the web, designed t-shirts, stickers, story boarded, animated, painted portraits, murals, done special fx makeup, sculpted, sewn bags and even made props and replica weapons. 

Right now I'm open for portrait commissions and available for freelance work, so feel free to [contact me](/page/contact-me) and chat about some cool ideas. I also have some of my hand made items up for sale, like bags and purses and even plushies.

I don't have any of my work exhibited currently, but I will be attending __[Fancon](https://www.fancon.co.za/)__ in Cape Town for the 2nd year running and you'll be able to find my prints, stickers and  even a copy of my self published __[book of doodles](/sketchbook/comicbook)__ there. Come say hi!

In the mean time, enjoy perusing my oddities